/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.yili.entity.YiliEmployee;
import org.apache.ibatis.annotations.Param;

/**
 * 伊利员工社保信息表DAO接口
 * @author liuruijun
 * @version 2020-07-29
 */
@MyBatisDao
public interface YiliEmployeeDao extends CrudDao<YiliEmployee> {
	int getEmployeeCount();
	YiliEmployee getByName(@Param("name") String name);
}