/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.yili.service;

import com.jsite.common.persistence.Page;
import com.jsite.common.service.CrudService;
import com.jsite.modules.yili.dao.YiliEmployeeDao;
import com.jsite.modules.yili.dao.YiliSocialSecurityInfoDao;
import com.jsite.modules.yili.entity.YiliEmployee;
import com.jsite.modules.yili.entity.YiliSocialSecurityInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 伊利员工社保信息表Service
 * @author liuruijun
 * @version 2020-07-29
 */
@Service
@Transactional(readOnly = true)
public class YiliEmployeeService extends CrudService<YiliEmployeeDao, YiliEmployee> {

	@Autowired
	private YiliSocialSecurityInfoDao yiliSocialSecurityInfoDao;
	
	@Override
	public YiliEmployee get(String id) {
		YiliEmployee yiliEmployee = super.get(id);
		yiliEmployee.setYiliSocialSecurityInfoList(yiliSocialSecurityInfoDao.findList(new YiliSocialSecurityInfo(yiliEmployee)));
		return yiliEmployee;
	}

	public int getEmployeeCount() {
		return dao.getEmployeeCount();
	}

	public YiliEmployee getByName(String name) {
		return dao.getByName(name);
	}

	@Override
	public List<YiliEmployee> findList(YiliEmployee yiliEmployee) {
		return super.findList(yiliEmployee);
	}
	
	@Override
	public Page<YiliEmployee> findPage(Page<YiliEmployee> page, YiliEmployee yiliEmployee) {
		return super.findPage(page, yiliEmployee);
	}
	
	@Override
	@Transactional(readOnly = false)
	public void save(YiliEmployee yiliEmployee) {
		super.save(yiliEmployee);
//		for (YiliSocialSecurityInfo yiliSocialSecurityInfo : yiliEmployee.getYiliSocialSecurityInfoList()){
//			if (yiliSocialSecurityInfo.getId() == null){
//				continue;
//			}
//			if (YiliSocialSecurityInfo.DEL_FLAG_NORMAL.equals(yiliSocialSecurityInfo.getDelFlag())){
//				if (StringUtils.isBlank(yiliSocialSecurityInfo.getId())){
//					yiliSocialSecurityInfo.setEmployeeId(yiliEmployee);
//					yiliSocialSecurityInfo.preInsert();
//					yiliSocialSecurityInfoDao.insert(yiliSocialSecurityInfo);
//				}else{
//					yiliSocialSecurityInfo.preUpdate();
//					yiliSocialSecurityInfoDao.update(yiliSocialSecurityInfo);
//				}
//			}else{
//				yiliSocialSecurityInfoDao.delete(yiliSocialSecurityInfo);
//			}
//		}
	}
	
	@Override
	@Transactional(readOnly = false)
	public void delete(YiliEmployee yiliEmployee) {
		super.delete(yiliEmployee);
		yiliSocialSecurityInfoDao.delete(new YiliSocialSecurityInfo(yiliEmployee));
	}
	
}