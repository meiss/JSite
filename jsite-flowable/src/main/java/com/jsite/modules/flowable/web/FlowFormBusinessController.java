/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.web;

import com.jsite.common.config.Global;
import com.jsite.common.lang.StringUtils;
import com.jsite.common.persistence.Page;
import com.jsite.common.web.BaseController;
import com.jsite.modules.flowable.entity.FlowFormBusiness;
import com.jsite.modules.flowable.service.FlowFormBusinessService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 流程表单业务总表Controller
 * @author liuruijun
 * @version 2019-04-11
 */
@Controller
@RequestMapping(value = "${adminPath}/flowable/flowFormBusiness")
public class FlowFormBusinessController extends BaseController {

	@Autowired
	private FlowFormBusinessService flowFormBusinessService;
	
	@ModelAttribute
	public FlowFormBusiness get(@RequestParam(required=false) String id) {
		FlowFormBusiness entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = flowFormBusinessService.get(id);
		}
		if (entity == null){
			entity = new FlowFormBusiness();
		}
		return entity;
	}
	
	@RequiresPermissions("flowable:flowFormBusiness:view")
	@RequestMapping(value = {"list", ""})
	public String list() {
		return "modules/flowable/flowFormBusinessList";
	}
	
	@RequiresPermissions("flowable:flowFormBusiness:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<FlowFormBusiness> listData(FlowFormBusiness flowFormBusiness, HttpServletRequest request, HttpServletResponse response) {
		Page<FlowFormBusiness> page = flowFormBusinessService.findPage(new Page<>(request, response), flowFormBusiness);
		return page;
	}

	@RequiresPermissions("flowable:flowFormBusiness:view")
	@RequestMapping(value = "form")
	public String form(FlowFormBusiness flowFormBusiness, Model model) {
		model.addAttribute("flowFormBusiness", flowFormBusiness);
		return "modules/flowable/flowFormBusinessForm";
	}

	@RequiresPermissions("flowable:flowFormBusiness:edit")
	@RequestMapping(value = "save")
	@ResponseBody
	public String save(FlowFormBusiness flowFormBusiness) {
		flowFormBusinessService.save(flowFormBusiness);
		return renderResult(Global.TRUE, "保存流程表单业务成功");
	}
	
	@RequiresPermissions("flowable:flowFormBusiness:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(FlowFormBusiness flowFormBusiness) {
		flowFormBusinessService.delete(flowFormBusiness);
		return renderResult(Global.TRUE, "删除流程表单业务成功");
	}

}